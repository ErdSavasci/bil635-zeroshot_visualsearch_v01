# DEV Branch

# BIL635 Course Project: ZeroShot VisualSearch Implementation
BIL635 - Modified Version of "Finding a Target with Zero-shot Invariant and Efficient Visual Search"

This project is based on "Finding any Waldo with Zero-shot Invariant and Efficient Visual Search" and created for BIL635 Final Project.

# Links
Original Project Github Page
https://github.com/kreimanlab/VisualSearchZeroShot

Original Article (Finding any Waldo with zero-shot invariant and efficient visual search)
https://www.nature.com/articles/s41467-018-06217-x

Supportive Article (Zero-Shot Learning - A Comprehensive Evaluation of the Good, the Bad and the Ugly)
https://arxiv.org/abs/1707.00600

Training Zero-Shot Model with Attributes
https://github.com/dfan/awa2-zero-shot-learning

# Other Used Scripts/Utilities
Edf2Mat Matlab Toolbox
https://github.com/uzh/edf-converter

Google/Bing Images Web Downloader
https://github.com/ultralytics/google-images-download

Python Script to download hundreds of images from 'Google Images'. It is a ready-to-run code! 
https://github.com/hardikvasa/google-images-download

Set of modules and datasets for visual recognition (heatmap_overlay.m)
https://github.com/sergeyk/vislab

ScanMatch - A Novel Method for Comparing Fixation Sequences
https://seis.bristol.ac.uk/~psidg/ScanMatch/

structfind
https://www.mathworks.com/matlabcentral/fileexchange/29808-structfind

# Author
Erdem SAVASCI
