In all scripts "Plot_*.m"

One can toggle "type" variable to generate plots for different tasks:
e.g.

type = 'array'
type = 'naturaldesign'
type = 'waldo'

