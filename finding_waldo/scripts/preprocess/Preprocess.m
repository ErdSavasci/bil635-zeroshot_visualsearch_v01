%% Author: Mengmi Zhang
%% Kreiman Lab
%% web: http://klab.tch.harvard.edu/
%% Date: April 5, 2018

%% Modified by Erdem SAVASCI

clear all;
close all;
clc;

objfileID = fopen('../../complete_dataset/cropped_img.txt','w');

for i = 1:240
    index = num2str(i);
    howManyZerosToBeAdded = 3 - length(index);
	while howManyZerosToBeAdded > 0
	   index = strcat('0', index);
	   howManyZerosToBeAdded = howManyZerosToBeAdded - 1;
	end

	trialname = ['img' index '.jpg'];
	img = imread(['../../complete_dataset/stimuli/img' index '.jpg']);
	img = imresize(img, [1028 1280]);

	fun = @(block_struct) imwrite(block_struct.data,['../../complete_dataset/cropped/img_id' trialname(4:end-4) '_' num2str(block_struct.location(1)) '_' num2str(block_struct.location(2)) '.jpg']);    
	blockproc(img,[224 224],fun);
    
	chopdir = dir(['../../complete_dataset/cropped/img_id' trialname(4:end-4) '_*.jpg']);
	for j = 1: length(chopdir)
		fprintf(objfileID,'%s\r\n',['cropped/' chopdir(j).name]);
	end
end

fclose(objfileID);

    
