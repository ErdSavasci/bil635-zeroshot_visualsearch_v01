%% Author: Mengmi Zhang
%% Kreiman Lab
%% web: http://klab.tch.harvard.edu/
%% Date: April 5, 2018

clear all;
close all;
clc;

% -- layers: numlayer, numtemplates, convsize
% -- layers: 5, 64, 14
% -- layers: 10, 128, 7
% -- layers: 17, 256, 4
% -- layers: 23, 512, 4
% -- layers: 24, 512, 2
% -- layers: 30, 512, 2
% -- layers: 31, 512, 1
LayerList = [1];
FixData = [];
Fix_posx = {};
Fix_posy = {};

buffer = fileread('../../targets_with_attributes/first_30_ones/data/classes.txt');
pattern = '(?<Column1>\d+)\s+(?<Column2>.*?)[\r\n]?' ;
classes = regexp(buffer, pattern, 'names');

receptiveSize = 200;
receptiveSize2 = 224;
arraysize = 80;
w = 1024;
h = 1280;

scoremat = zeros(240, arraysize);
for i = 1:30
    
    %trial = MyData(i);
    %trialname = trial.targetname;
    
    path = ['../../complete_dataset/groundtruth/gt' num2str(i) '.jpg' ];
    gt = imread(path);
    gt = imresize(gt,[w,h]);
    gt = mat2gray(gt);
    gt = im2bw(gt,0.5);
    gt = double(gt);
    
    img = imread(['../../complete_dataset/stimuli/img' sprintf('%03d',i) '.jpg']);
    img = imresize(img, [w, h]);
    display(['img: ' num2str(i)]);
    size(img);
    piecedir = dir(['../../complete_dataset/cropped/img_id' sprintf('%03d',i) '_*_layertopdown.mat']);
    wholeimg = zeros(length(LayerList),size(img,1), size(img,2));
    %overallimg = zeros(size(img,1),size(img,2),3);
    
    posx = [];
    posy = [];
    
    for l = 1: length(LayerList)
        for j = 1: length(piecedir)
            %j
            input = load(['../../complete_dataset/cropped/' piecedir(j).name]);
            comp = imread(['../../complete_dataset/cropped/' piecedir(j).name(1:end-17) '.jpg']);
            input = input.x;
            input = imresize(input, [size(comp,1) size(comp,2)]);
            C = strsplit(piecedir(j).name,'_');        
            startpos = str2num(C{3});
            endpos = str2num(C{4});
            wholeimg(l,startpos: startpos+size(comp,1) - 1, endpos: endpos+size(comp,2) - 1) = input;
            overallimg(startpos: startpos+size(comp,1) - 1, endpos: endpos+size(comp,2) - 1,:) = comp;
            wholeimg1 = squeeze(mat2gray( wholeimg(l,:,:)));

% Visualizing heatmap of cropped pieces on the stimuli
%             subplot(1,2,1);
%             imshow(wholeimg1);
%             subplot(1,2,2);
%             imshow(uint8(overallimg));
%             pause;
%             imshow(heatmap_overlay(img,wholeimg1));
%             pause(0.05);
        end
        wholeimg(l,:,:) = mat2gray(wholeimg(l,:,:));
    end
    
    wholeimg = squeeze(mean(wholeimg,1));
    
    
    % Showing the target object    
    %subplot(1,2,1);
    %imshow(heatmap_overlay(gt,wholeimg));
    %subplot(1,2,2);
    %imshow(gt);
    %pause(0.010);
    
    %apply IOR
    found = 0;
    fixtime = 1;
    salimg = wholeimg;
    
    class = classes(i);
    for f = 1: arraysize

        [Y,idx] = max(salimg(:));
        [x y]= ind2sub(size(salimg),idx);
        
        posx = [posx; x];
        posy = [posy; y];
        
         %subplot(2,2,1);
         %imshow(salimg);
         %hold on;
         %plot(y,x,'b*','markers',20);
         %hold off;
         %drawnow;
         %subplot(2,2,2);
         %imshow(gt);
         %subplot(2,2,3);
         %imshow(img);
%        pause(0.2);
        
        calc_x = round(x / receptiveSize2); 
        start_x = receptiveSize2 * calc_x;
        calc_y = round(y / receptiveSize2); 
        start_y = receptiveSize2 * calc_y;
        end_x = start_x + receptiveSize2;
        end_y = start_y + receptiveSize2;
        
        if start_y < 1
            start_y = 1;
        end
        if start_x < 1
            start_x = 1;
        end
        if end_y > size(img,2)
            end_y = size(img,2);
        end
        if end_x > size(img,1)
            end_x = size(img,1);
        end

        fixatedPlace_leftx = x - receptiveSize/2 + 1;
        fixatedPlace_rightx = x + receptiveSize/2;
        fixatedPlace_lefty = y - receptiveSize/2 + 1;
        fixatedPlace_righty = y + receptiveSize/2;
        
        if fixatedPlace_leftx < 1
            fixatedPlace_leftx = 1;
        end
        if fixatedPlace_lefty < 1
            fixatedPlace_lefty = 1;
        end
        if fixatedPlace_rightx > size(img,1)
            fixatedPlace_rightx = size(img,1);
        end
        if fixatedPlace_righty > size(img,2)
            fixatedPlace_righty = size(img,2);
        end      
		
        % grayii = rgb2gray(img);
		% Python Evaluation
        %savefixatedPlace = img(fixatedPlace_leftx:fixatedPlace_rightx, fixatedPlace_lefty:fixatedPlace_righty, :);
        %imwrite(savefixatedPlace, ['../../complete_dataset/cropped_targets_for_eval/img' sprintf('%03d',i) '_' num2str(fixtime) '.jpg']);
         
		% Real Evaluation 
		%fixatedPlace = gt(fixatedPlace_leftx:fixatedPlace_rightx, fixatedPlace_lefty:fixatedPlace_righty);
        fixatedPlace = gt(start_x:end_x, start_y:end_y);
		 
        if sum(sum(fixatedPlace)) > 0 
            % To capture images for testing the Python script
            savefixatedPlace = img(start_x:end_x, start_y:end_y, :);
            imshow(savefixatedPlace)
            imwrite(savefixatedPlace, ['../../complete_dataset/cropped_targets_for_eval/img' sprintf('%03d',i) '.jpg']);
            
            found = 1;
            break;
        else         
            found = 0;
                        
            fixtime = fixtime + 1;
            salimg(fixatedPlace_leftx:fixatedPlace_rightx, fixatedPlace_lefty:fixatedPlace_righty) = 0;
            salimg = mat2gray(salimg);
        end
        
    end
    
    Fix_posx = [Fix_posx posy'];
    Fix_posy = [Fix_posy posx'];
    
    display(['img id: '  num2str(i) '; fix found at time step: ' num2str(fixtime) ]);
    if fixtime <= arraysize
        scoremat(i,fixtime) = 1;
    end
    
end

Fix.FixX = Fix_posx;
Fix.FixY = Fix_posy;
% topdown30_31_croppednaturaldesign.mat
save(['../../complete_dataset/data_for_plot/topdown30_31_croppednaturaldesign.mat'],'scoremat','Fix');
plot(cumsum(mean(scoremat,1)));
xlabel('fixation number');
ylabel('cummulative performance');

