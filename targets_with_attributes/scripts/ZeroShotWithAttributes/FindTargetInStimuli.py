import sys
import argparse
import numpy as np
import torch
import torch.nn as nn
import torchvision
import platform
import torchvision.transforms as transforms

from PIL import Image, ImageDraw

plt = platform.system()


def make_batches(x, bs):
    if x <= bs:
        return [min(x, bs)]
    else:
        return [bs] + make_batches(x - bs, bs)


def create_new_weights(original_weights, n_channels):
    dst = torch.zeros(64, 4, 7, 7)
    # Repeat original weights up to fill dimension
    start = 0
    for i in make_batches(n_channels, 3):
        # print('dst',start,start+i, ' = src',0,i)
        dst[:, start:start + i, :, :] = original_weights[:, :i, :, :]
        start = start + i
    return dst


def build_model(num_labels, is_pretrained, is_parallel, device):
    model = torchvision.models.resnet50(pretrained=is_pretrained).to(device)
    if is_pretrained:
        for i, param in model.named_parameters():
            param.requires_grad = False
    if is_parallel:
        model = nn.DataParallel(model)
        model_features = model.module.fc.in_features
        model.module.fc = nn.Sequential(nn.BatchNorm1d(model_features), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                        nn.Linear(model_features, 1000),
                                        nn.BatchNorm1d(1000), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                        nn.Linear(1000, num_labels))
    else:
        model_features = model.fc.in_features
        model.fc = nn.Sequential(nn.BatchNorm1d(model_features), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                 nn.Linear(model_features, 1000),
                                 nn.BatchNorm1d(1000), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                 nn.Linear(1000, num_labels))

    original_weights = model.conv1.weight.clone()
    new_weights = create_new_weights(original_weights, 4)
    new_layer = nn.Conv2d(4, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
    new_layer.weight = nn.Parameter(new_weights, False)
    model.conv1 = new_layer

    if device.type == 'cuda':
        model.cuda()

    return model


def load_model(model_param, model_file, device):
    is_parallel = torch.cuda.device_count() > 1
    if is_parallel:
        model_param = nn.DataParallel(model_param)
        dictionary = torch.load(model_file)
        model_param = model_param.module
        model_param.load_state_dict(dictionary)
    else:
        state_dict = torch.load(model_file)
        model_param.load_state_dict(state_dict)

    model_param.to(device)

    return model_param


def label_to_class(pred_labels, predicate_binary_mat, classes, t_classes, mode):
    curr_labels = pred_labels[0, :].cpu().detach().numpy()
    best_dist = sys.maxsize
    best_index = -1
    for j in range(predicate_binary_mat.shape[0]):
        class_labels = predicate_binary_mat[j, :]

        if mode == 'train' and classes[j] in t_classes:
            dist = get_euclidean_dist(curr_labels, class_labels)
            if dist < best_dist:
                best_index = j
                best_dist = dist
        elif mode == 'test' and classes[j] not in t_classes:
            dist = get_hamming_dist(curr_labels, class_labels)
            if dist < best_dist:
                best_index = j
                best_dist = dist
        elif mode == 'test2' and classes[j] not in t_classes:
            dist = get_euclidean_dist(curr_labels, class_labels)
            if dist < best_dist:
                best_index = j
                best_dist = dist

    return best_dist, best_index


def get_hamming_dist(curr_labels, class_labels):
    return np.sum(curr_labels != class_labels)


def get_euclidean_dist(curr_labels, class_labels):
    return np.sqrt(np.sum((curr_labels - class_labels) ** 2))


def main():
    if plt == 'Windows':
        root_path = 'E:/BIL635-FinalProject/ErdSavasci-BIL635-ZeroShot_VisualSearch/targets_with_attributes' \
                    '/first_30_ones/'
    elif plt == 'Linux':
        root_path = '/home/erdsavasci/bil635-zeroshot_visualsearch/targets_with_attributes' \
                    '/first_30_ones/'
    else:
        sys.exit(0)

    predicate_binary_mat = np.array(np.genfromtxt(root_path + 'data/predicate-matrix-binary.txt', dtype='int'))
    classes = np.array(np.genfromtxt(root_path + 'data/classes.txt', dtype='str'))[:, -1]
    train_classes = np.array(np.genfromtxt(root_path + 'data/trainclasses.txt', dtype='str'))
    test_classes = np.array(np.genfromtxt(root_path + 'data/testclasses.txt', dtype='str'))

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    stimuli_width = 1280
    stimuli_height = 1024
    target_view = 224
    padding_view = 56

    loaded_model = build_model(predicate_binary_mat.shape[1], False, False, device)
    loaded_model = load_model(loaded_model, root_path + 'output/models/model.bin', device)

    loaded_model.train(False)
    loaded_model.eval()

    for class_index in range(len(classes)):
        # Target Name
        to_be_found_class_name = classes[class_index]
        image_orig = Image.open(root_path + 'data/Stimuli/V2/img' + str(class_index + 1).zfill(3) + '.png')\
            .convert('RGBA')
        image = np.array(image_orig)

        target_found = False
        target_ind_dists = dict()
        target_views = [448, 420, 392, 364, 336, 308, 280, 252, 224, 196, 168, 140, 112, 84, 56, 28]
        # target_views = [448, 434, 420, 406, 392, 378, 364, 350, 336, 312, 308, 294, 280, 266, 252, 228, 224, 210,
        #                             196, 182, 168, 154, 140, 126, 112, 98, 84, 70, 56, 42, 28, 14]

        for view_index in range(len(target_views)):
            target_view = target_views[view_index]
            padding_view = target_view
            start_x = 0
            end_x = target_view
            start_y = 0
            end_y = target_view

            while True:
                cropped_image_orig = image[start_x:end_x, start_y:end_y, :]
                test_process_steps = transforms.Compose([
                    transforms.Resize((224, 224)),
                    transforms.ToTensor()
                ])
                cropped_image_orig = Image.fromarray(cropped_image_orig, 'RGBA')
                cropped_image = test_process_steps(cropped_image_orig)
                cropped_image = cropped_image.to(device)
                cropped_image = cropped_image.reshape([1, 4, 224, 224])

                outputs = loaded_model(cropped_image)
                sigmoid_outputs = torch.sigmoid(outputs)
                pred_labels = sigmoid_outputs

                if 0 <= class_index <= 19:
                    mode = 'train'
                else:
                    mode = 'test'

                if mode == 'test':
                    temp = pred_labels[:, :]
                    temp[temp >= 0.5] = 1
                    temp[temp < 0.5] = 0

                best_dist, best_index = label_to_class(pred_labels, predicate_binary_mat, classes, train_classes, mode)
                curr_pred_class = classes[best_index]
                print('[{}/{}] [{}/{}] To Be Found Class Name: {}|Current Target Prediction: {}'
                      .format(class_index + 1,
                              len(classes),
                              view_index + 1,
                              len(target_views),
                              to_be_found_class_name,
                              curr_pred_class))

                if curr_pred_class == to_be_found_class_name:
                    print('Target Found!')
                    target_found = True
                    target_ind_dists[best_dist] = [cropped_image_orig, start_x, end_x, start_y, end_y]
                    print('[{}/{}] StartX: {} EndX: {} StartY: {} EndY: {}'.format(view_index + 1, len(target_views),
                                                                                   start_x, end_x, start_y, end_y))

                start_y = start_y + padding_view
                end_y = start_y + target_view
                if start_y > stimuli_width:
                    start_y = 0
                    end_y = target_view
                    start_x = start_x + padding_view
                    end_x = start_x + target_view

                if end_y > stimuli_width:
                    end_y = stimuli_width

                if end_x > stimuli_height:
                    end_x = stimuli_height

                if start_x > stimuli_height:
                    break

                if end_y - start_y > (end_x - start_x) * 1.25:
                    break

                if end_x - start_x > (end_y - start_y) * 1.25:
                    start_y = 0
                    end_y = target_view
                    start_x = start_x + padding_view
                    end_x = start_x + target_view

        if target_found:
            best_dist = sys.maxsize

            for key, value in target_ind_dists.items():
                if key < best_dist:
                    best_dist = key

            # Debug Purpose Only
            # target_ind_dists[best_dist][0].show()

            draw = ImageDraw.Draw(image_orig)
            draw.rectangle([target_ind_dists[best_dist][3], target_ind_dists[best_dist][1],
                            target_ind_dists[best_dist][4], target_ind_dists[best_dist][2]], outline=(255, 255, 0, 255),
                           width=10)
            image_orig.show()

            # Clear variables
            del draw
            del target_ind_dists

    sys.exit(0)


if __name__ == '__main__':
    main()
