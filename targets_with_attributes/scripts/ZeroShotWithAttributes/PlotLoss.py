import matplotlib.pyplot as plt
import numpy as np
import os
import re


# Plot loss from saved cluster output
def plot_loss(root_path, mode):
    losses = None

    if mode == 'train':
        f = open(root_path + 'output/output_train_losses.txt', 'r')
        losses = [float(num) for num in re.findall(r'Training Batch Loss: (.*)', f.read())]
    elif mode == 'test':
        f = open(root_path + 'output/output_test_losses.txt', 'r')
        losses = [float(num) for num in re.findall(r'Test Loss: (.*)', f.read())]

    iterations = [1 + 50 * i for i in range(len(losses))]

    # Plot loss and accuracy curves
    plt.plot(iterations, losses, label='Binary Cross-Entropy Loss')
    plt.yticks(np.arange(0, max(losses), 0.1))
    plt.legend()
    plt.xlabel('Iterations', fontweight='bold')
    plt.ylabel('Training Batch' if mode == 'train' else 'Test' + ' Loss', fontweight='bold')

    if not os.path.exists(root_path + 'output/figures'):
        os.mkdir(root_path + 'output/figures')
    plt.savefig(root_path + 'output/figures/loss_plot_' + mode + '.png', dpi=500)
    plt.close()


if __name__ == '__main__':
    _root_path = ''

    if plt == 'Windows':
        _root_path = 'E:/BIL635-FinalProject/ErdSavasci-BIL635-ZeroShot_VisualSearch/targets_with_attributes' \
                    '/first_30_ones/'
    elif plt == 'Linux':
        _root_path = '/home/erdsavasci/bil635-zeroshot_visualsearch/targets_with_attributes' \
                    '/first_30_ones/'
    else:
        exit(0)

    plot_loss(_root_path, 'train')
    plot_loss(_root_path, 'test')
