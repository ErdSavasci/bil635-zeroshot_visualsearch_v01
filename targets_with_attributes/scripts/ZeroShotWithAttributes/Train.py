import torch.onnx
import platform
import itertools
import math
from torch.optim.adam import Adam as Adam
from torch.optim.adagrad import Adagrad as Adagrad

from Utils import *

device = None
train_classes = None
test_classes = None
classes = None
predicate_binary_mat = None
predicate_continuous_mat = None
num_labels = None
train_dataset = None
modified_train_dataset = None
test_dataset = None
modified_test_dataset = None
single_train_classes_test_dataset = None
single_test_classes_test_dataset = None
plt = platform.system()


def check_mem():
    mem = [0, 0]

    if plt == 'Windows':
        mem = os.popen(
            '"C:/Program Files/NVIDIA Corporation/NVSMI/nvidia-smi" '
            '--query-gpu=memory.total,memory.used --format=csv,nounits,noheader').read().split(",")
    elif plt == 'Linux':
        mem = os.popen(
            '"/usr/bin/nvidia-smi" '
            '--query-gpu=memory.total,memory.used --format=csv,nounits,noheader').read().split(",")

    return mem


def initialize_params(root_path):
    global device
    global train_classes
    global test_classes
    global classes
    global predicate_binary_mat
    global predicate_continuous_mat
    global num_labels
    global train_dataset
    global modified_train_dataset
    global test_dataset
    global modified_test_dataset
    global single_train_classes_test_dataset
    global single_test_classes_test_dataset

    total, used = check_mem()
    total = int(total)
    used = int(used)

    print('Free Memory: ' + str(total - used))

    # max_mem = int(total * 0.8)
    # block_mem = max_mem - used
    # temp_variable = torch.rand((256, 1024, block_mem)).cuda()
    # temp_variable = torch.rand((2, 2)).cuda()

    # cuda:0
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    train_classes = np.array(np.genfromtxt(root_path + 'data/trainclasses.txt', dtype='str'))
    test_classes = np.array(np.genfromtxt(root_path + 'data/testclasses.txt', dtype='str'))
    classes = np.array(np.genfromtxt(root_path + 'data/classes.txt', dtype='str'))[:, -1]
    predicates = np.array(np.genfromtxt(root_path + 'data/predicates.txt', dtype='str'))[:, -1]
    predicate_binary_mat = np.array(np.genfromtxt(root_path + 'data/predicate-matrix-binary.txt', dtype='int'))
    # predicate_continuous_mat = np.array(np.genfromtxt('data/predicate-matrix-continuous.txt', dtype='float'))
    num_labels = len(predicates)

    train_process_steps = transforms.Compose([
        transforms.RandomRotation(15),
        transforms.RandomHorizontalFlip(),
        transforms.ColorJitter(brightness=0.3, contrast=0.3),
        # transforms.ColorJitter(brightness=float("{0:.2f}".format(random.uniform(0.4, 0.6))),
        #                       contrast=float("{0:.2f}".format(random.uniform(0.4, 0.6))),
        #                       saturation=float("{0:.2f}".format(random.uniform(0.4, 0.6))),
        #                       hue=float("{0:.2f}".format(random.uniform(0.3, 0.5)))),
        transforms.Resize((224, 224)),  # ImageNet standard
        transforms.ToTensor()
    ])
    test_process_steps = transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor()
    ])

    train_dataset = TargetDataset(root_path, 'trainclasses.txt', test_process_steps, False, False)
    modified_train_dataset = TargetDataset(root_path, 'trainclasses.txt', test_process_steps, False, True)
    test_dataset = TargetDataset(root_path, 'testclasses.txt', test_process_steps, False, False)
    modified_test_dataset = TargetDataset(root_path, 'testclasses.txt', test_process_steps, False, True)
    test_dataset = data.ConcatDataset([test_dataset, modified_test_dataset])
    single_train_classes_test_dataset = TargetDataset(root_path, 'trainclasses.txt', test_process_steps, True, False)
    single_test_classes_test_dataset = TargetDataset(root_path, 'testclasses.txt', test_process_steps, True, False)


def train(root_path, num_epochs, eval_interval, learning_rate, output_filename, model_name, optimizer_name, batch_size,
          verbose, stop_when_limit_reached):
    if batch_size < 2:
        return

    org_batch_size = batch_size // 2
    rem_batch_size = batch_size - org_batch_size
    train_params = {'batch_size': org_batch_size, 'shuffle': True, 'num_workers': 3}
    modified_train_params = {'batch_size': rem_batch_size, 'shuffle': True, 'num_workers': 3}
    test_params = {'batch_size': 1, 'shuffle': True, 'num_workers': 3}

    # noinspection PyTypeChecker
    train_loader = data.DataLoader(train_dataset, **train_params)
    # noinspection PyTypeChecker
    modified_train_loader = data.DataLoader(modified_train_dataset, **modified_train_params)
    # noinspection PyTypeChecker
    test_loader = data.DataLoader(test_dataset, **test_params)
    # noinspection PyTypeChecker
    single_train_classes_test_loader = data.DataLoader(single_train_classes_test_dataset, **test_params)
    # noinspection PyTypeChecker
    single_test_classes_test_loader = data.DataLoader(single_test_classes_test_dataset, **test_params)

    criterion = nn.BCELoss()  # nn.BCELoss()

    if torch.cuda.device_count() > 1:
        model = build_model(num_labels, False, True, device).to(device)
    else:
        model = build_model(num_labels, False, False, device).to(device)
    optimizer = Adagrad(model.parameters(), lr=learning_rate, weight_decay=0.001)
    curr_acc_train = 0.0
    curr_acc_test = 0.0
    single_curr_acc_train = 0.0
    single_curr_acc_test = 0.0

    for epoch in range(num_epochs):
        total_steps = len(train_loader)
        model.train(True)

        data_iter = iter(modified_train_loader)
        total_scanned_images_in_train_dataset = 0
        for i, (images, features, img_names, indexes) in enumerate(train_loader):
            total_scanned_images_in_train_dataset = total_scanned_images_in_train_dataset + len(images)

            try:
                modified_images, modified_features, modified_img_names, modified_indexes = data_iter.__next__()
                temp_list = [images, modified_images]
                images = torch.cat(temp_list)
                temp_list = [features, modified_features]
                features = torch.cat(temp_list)
                temp_list = [indexes, modified_indexes]
                indexes = torch.cat(temp_list)
                temp_list = list(img_names)
                temp_list = temp_list + list(modified_img_names)
                img_names = tuple(temp_list)
                total_scanned_images_in_train_dataset = total_scanned_images_in_train_dataset + len(modified_images)
            except StopIteration:
                print('Reached end of the modified training dataset')

            images = images.to(device)
            features = features.to(device).float()

            outputs = model(images)
            sigmoid_outputs = torch.sigmoid(outputs)
            loss = criterion(sigmoid_outputs, features)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            # if i % 50 == 0:
            # curr_iter = epoch * len(train_loader) + i
            if verbose:
                print(
                    'LAST TRAIN ACC: {} Epoch [{}/{}], Step [{}/{}], Training Batch Loss: {:.4f}'.format(curr_acc_train,
                                                                                                         epoch + 1,
                                                                                                         num_epochs,
                                                                                                         i + 1,
                                                                                                         total_steps,
                                                                                                         loss.item()))
                with open(root_path + 'output/output_train_losses.txt', 'a') as f:
                    f.write(
                        'Epoch [{}/{}], Step [{}/{}], Training Batch Loss Loss: {:.4f}'.format(epoch + 1, num_epochs,
                                                                                               i + 1, total_steps,
                                                                                               loss.item()))
            sys.stdout.flush()

        total_steps = len(test_loader)
        model.train(False)
        model.eval()

        total_scanned_images_in_test_dataset = 0
        with torch.no_grad():
            for i, (images, features, img_names, indexes) in enumerate(test_loader):
                total_scanned_images_in_test_dataset = total_scanned_images_in_test_dataset + len(images)

                images = images.to(device)
                features = features.to(device).float()

                outputs = model(images)
                sigmoid_outputs = torch.sigmoid(outputs)
                loss = criterion(sigmoid_outputs, features)

                if verbose:
                    print(
                        'LAST TEST ACC: {} Epoch [{}/{}], Step [{}/{}], Test Loss: {:.4f}'.format(curr_acc_test,
                                                                                                  epoch + 1,
                                                                                                  num_epochs, i + 1,
                                                                                                  total_steps,
                                                                                                  loss.item()))
                    with open(root_path + 'output/output_test_losses.txt', 'a') as f:
                        f.write('Epoch [{}/{}], Step [{}/{}], Test Loss: {:.4f}'.format(epoch + 1, num_epochs, i + 1,
                                                                                        total_steps, loss.item()))
                    sys.stdout.flush()

        # Clearing variables
        del images
        del features
        del img_names
        del indexes
        del outputs
        del sigmoid_outputs
        del loss
        del data_iter

        print('Total scanned images in training dataset: {}'.format(total_scanned_images_in_train_dataset))
        print('Total scanned images in test dataset: {}'.format(total_scanned_images_in_test_dataset))

        model.train(False)
        model.eval()

        # Do some evaluations
        if (epoch + 1) % eval_interval == 0:
            print('Evaluating:')
            curr_acc_train = evaluate(model, train_loader, device, classes, predicate_binary_mat, train_classes,
                                      'train')
            print('Epoch [{}/{}] Approx. training accuracy: {}'.format(epoch + 1, num_epochs, curr_acc_train))
            curr_acc_test = evaluate(model, test_loader, device, classes, predicate_binary_mat, train_classes,
                                     'test')
            print('Epoch [{}/{}] Approx. test accuracy: {}'.format(epoch + 1, num_epochs, curr_acc_test))
            print('Evaluating actual single images:')
            single_curr_acc_train = evaluate(model, single_train_classes_test_loader, device, classes,
                                             predicate_binary_mat, train_classes, 'test')
            print('Epoch [{}/{}] Approx. single images training accuracy: {}'.format(epoch + 1, num_epochs,
                                                                                     single_curr_acc_train))
            single_curr_acc_test = evaluate(model, single_test_classes_test_loader, device, classes,
                                            predicate_binary_mat, train_classes, 'test')
            print('Epoch [{}/{}] Approx. single images test accuracy: {}'.format(epoch + 1, num_epochs,
                                                                                 single_curr_acc_test))

        if stop_when_limit_reached and single_curr_acc_train >= 0.9 and single_curr_acc_test >= 0.5:
            break

    # Make final predictions
    print('Making predictions:')
    if not os.path.exists(root_path + 'output/models'):
        os.mkdir(root_path + 'output/models')
    torch.save(model.state_dict(), root_path + 'output/models/{}'.format(model_name))
    torch.save(optimizer.state_dict(), root_path + 'output/models/{}'.format(optimizer_name))
    revert_model_back = False
    if next(model.parameters()).is_cuda:
        revert_model_back = True
        model.cpu()
    # noinspection PyTypeChecker
    torch.onnx.export(model, torch.randn(batch_size, 4, 224, 224),
                      root_path + 'output/models/{}'.format('my_resnet50.onnx'),
                      export_params=True, dynamic_axes={'input': {0: 'batch_size'}, 'output': {0: 'batch_size'}})
    if revert_model_back:
        model.cuda()
    make_predictions(root_path, model, train_loader, output_filename, device, predicate_binary_mat, classes,
                     train_classes, 'train', False, verbose)
    make_predictions(root_path, model, test_loader, output_filename, device, predicate_binary_mat, classes,
                     train_classes, 'test', False, verbose)
    make_predictions(root_path, model, single_train_classes_test_loader, output_filename + '_single', device,
                     predicate_binary_mat, classes, train_classes, 'train', True, verbose)
    make_predictions(root_path, model, single_test_classes_test_loader, output_filename + '_single', device,
                     predicate_binary_mat, classes, train_classes, 'test', True, verbose)
