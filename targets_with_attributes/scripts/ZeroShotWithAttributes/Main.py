from Train import *

import argparse

plt = platform.system()


def arg2bool(arg):
    if isinstance(arg, bool):
        return arg
    elif arg.lower() in ('yes', 'true', 'y', 't', 1):
        return True
    elif arg.lower() in ('no', 'false', 'n', 'f', 0):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def main():
    if plt == 'Windows':
        root_path = 'E:/BIL635-FinalProject/ErdSavasci-BIL635-ZeroShot_VisualSearch/targets_with_attributes' \
                    '/first_30_ones/'
    elif plt == 'Linux':
        root_path = '/home/erdsavasci/bil635-zeroshot_visualsearch/targets_with_attributes' \
                    '/first_30_ones/'
    else:
        sys.exit(0)

    with open(root_path + 'output/output_train_losses.txt', 'w'):
        pass
    with open(root_path + 'output/output_test_losses.txt', 'w'):
        pass

    parser = argparse.ArgumentParser()
    parser.add_argument('--num_epochs', '-n', type=int, default=100)
    parser.add_argument('--eval_interval', '-et', type=int, default=1)
    parser.add_argument('--learning_rate', '-lr', type=float, default=0.0025)
    parser.add_argument('--model_name', '-mn', type=str, default='model.bin')
    parser.add_argument('--optimizer_name', '-opt', type=str, default='optimizer.bin')
    parser.add_argument('--output_file', '-o', type=str, default='predictions.txt')
    parser.add_argument('--stop_when_limit_reach', '-s', type=arg2bool, nargs='?', const=True, default=True)
    if plt == 'Windows':
        parser.add_argument('--batch_size', '-bs', type=int, default=4)
    elif plt == 'Linux':
        parser.add_argument('--batch_size', '-bs', type=int, default=32)
    parser.add_argument('--verbose', '-v', type=arg2bool, nargs='?', const=True, default=False)
    args = parser.parse_args()
    args = vars(args)

    num_epochs = args['num_epochs']
    eval_interval = args['eval_interval']
    learning_rate = args['learning_rate']
    model_name = args['model_name']
    optimizer_name = args['optimizer_name']
    output_filename = args['output_file']
    batch_size = args['batch_size']
    verbose = args['verbose']
    stop_when_limit_reach = args['stop_when_limit_reach']

    initialize_params(root_path)

    train(root_path, num_epochs, eval_interval, learning_rate, output_filename, model_name, optimizer_name, batch_size,
          verbose, stop_when_limit_reach)

    # debug('models/model.bin', 'evaluate')
    sys.exit(0)


# Sample usage: `python Train.py -n 25 -et 5 -lr 0.000025 -bs 24`
if __name__ == '__main__':
    main()
