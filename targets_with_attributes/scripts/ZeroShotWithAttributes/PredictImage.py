import sys
import argparse
import numpy as np
import torch
import torch.nn as nn
import torchvision
import platform
import torchvision.transforms as transforms

from PIL import Image

plt = platform.system()


def make_batches(x, bs):
    if x <= bs:
        return [min(x, bs)]
    else:
        return [bs] + make_batches(x - bs, bs)


def create_new_weights(original_weights, n_channels):
    dst = torch.zeros(64, 4, 7, 7)
    # Repeat original weights up to fill dimension
    start = 0
    for i in make_batches(n_channels, 3):
        # print('dst',start,start+i, ' = src',0,i)
        dst[:, start:start + i, :, :] = original_weights[:, :i, :, :]
        start = start + i
    return dst


def build_model(num_labels, is_pretrained, is_parallel, device):
    model = torchvision.models.resnet50(pretrained=is_pretrained).to(device)
    if is_pretrained:
        for i, param in model.named_parameters():
            param.requires_grad = False
    if is_parallel:
        model = nn.DataParallel(model)
        model_features = model.module.fc.in_features
        model.module.fc = nn.Sequential(nn.BatchNorm1d(model_features), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                        nn.Linear(model_features, 1000),
                                        nn.BatchNorm1d(1000), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                        nn.Linear(1000, num_labels))
    else:
        model_features = model.fc.in_features
        model.fc = nn.Sequential(nn.BatchNorm1d(model_features), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                 nn.Linear(model_features, 1000),
                                 nn.BatchNorm1d(1000), nn.ReLU(inplace=True), nn.Dropout(0.5),
                                 nn.Linear(1000, num_labels))

    original_weights = model.conv1.weight.clone()
    new_weights = create_new_weights(original_weights, 4)
    new_layer = nn.Conv2d(4, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
    new_layer.weight = nn.Parameter(new_weights, False)
    model.conv1 = new_layer

    if device.type == 'cuda':
        model.cuda()

    return model


def load_model(model_param, model_file, device):
    is_parallel = torch.cuda.device_count() > 1
    if is_parallel:
        model_param = nn.DataParallel(model_param)
        dictionary = torch.load(model_file)
        model_param = model_param.module
        model_param.load_state_dict(dictionary)
    else:
        state_dict = torch.load(model_file)
        model_param.load_state_dict(state_dict)

    model_param.to(device)

    return model_param


def label_to_class(pred_labels, predicate_binary_mat, classes, t_classes, mode):
    curr_labels = pred_labels[0, :].cpu().detach().numpy()
    best_dist = sys.maxsize
    best_index = -1
    for j in range(predicate_binary_mat.shape[0]):
        class_labels = predicate_binary_mat[j, :]
        
        if mode == 'train' and classes[j] in t_classes:
            dist = get_euclidean_dist(curr_labels, class_labels)
            if dist < best_dist:
                best_index = j
                best_dist = dist
        elif mode == 'test' and classes[j] not in t_classes:
            dist = get_hamming_dist(curr_labels, class_labels)
            if dist < best_dist:
                best_index = j
                best_dist = dist

    return classes[best_index]


def get_hamming_dist(curr_labels, class_labels):
    return np.sum(curr_labels != class_labels)


def get_euclidean_dist(curr_labels, class_labels):
    return np.sqrt(np.sum((curr_labels - class_labels) ** 2))


def main():
    if plt == 'Windows':
        root_path = 'E:/BIL635-FinalProject/ErdSavasci-BIL635-ZeroShot_VisualSearch/targets_with_attributes' \
                    '/first_30_ones/'
    elif plt == 'Linux':
        root_path = '/home/erdsavasci/bil635-zeroshot_visualsearch/targets_with_attributes' \
                    '/first_30_ones/'
    else:
        sys.exit(0)

    predicate_binary_mat = np.array(np.genfromtxt(root_path + 'data/predicate-matrix-binary.txt', dtype='int'))
    classes = np.array(np.genfromtxt(root_path + 'data/classes.txt', dtype='str'))[:, -1]
    train_classes = np.array(np.genfromtxt(root_path + 'data/trainclasses.txt', dtype='str'))        

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    parser = argparse.ArgumentParser()
    parser.add_argument('--image_file_location', '-f', type=str, default='')
    parser.add_argument('--mode', '-m', type=str, default='test')
    args = parser.parse_args()
    args = vars(args)
    image_file_location = args['image_file_location']
    mode = args['mode']

    image = Image.open(image_file_location)
    test_process_steps = transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor()
    ])
    image = test_process_steps(image)
    image = image.to(device)
    image = image.reshape([1, 4, 224, 224])

    loaded_model = build_model(predicate_binary_mat.shape[1], False, False, device)
    loaded_model = load_model(loaded_model, root_path + 'output/models/model.bin', device)
    
    loaded_model.train(False)
    loaded_model.eval()

    outputs = loaded_model(image)
    sigmoid_outputs = torch.sigmoid(outputs)
    pred_labels = sigmoid_outputs

    if mode == 'test':
        temp = pred_labels[:, :]
        temp[temp >= 0.5] = 1
        temp[temp < 0.5] = 0

    curr_pred_class = label_to_class(pred_labels, predicate_binary_mat, classes, train_classes, mode)

    print('Class of given image is: ' + curr_pred_class)

    return curr_pred_class


if __name__ == '__main__':
    main()
