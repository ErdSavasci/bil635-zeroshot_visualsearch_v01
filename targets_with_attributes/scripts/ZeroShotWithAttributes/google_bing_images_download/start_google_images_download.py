import errno
import os
from glob import glob
from datetime import datetime

from google_bing_images_download import bing_scraper
# from google_bing_images_download import google_bing_images_download


def silent_remove_of_folder(folder):
    try:
        os.rmdir(folder)
    except OSError as e:
        if e.errno != errno.ENOENT:
            raise e
        return False
    return True


def start_download_images():
    root_path = 'E:/BIL635-FinalProject/ErdSavasci-BIL635-ZeroShot_VisualSearch/targets_with_attributes' \
                '/first_30_ones/data/Dataset'

    start_time = datetime.now().strftime("%H:%M:%S")
    print('Start Time: ' + start_time)
    classes = []
    folders = glob(root_path + '/*')
    for folder_full_name in folders:
        folder_name = folder_full_name[folder_full_name.index('\\') + 1:]

        files = glob(folder_full_name + '/*')
        if len(files) > 0 or folder_name != 'truck':
            continue

        print('Downloading ' + folder_name + ' images')

        output_folder = os.path.join(root_path, folder_name)

        arguments = {
            "keywords": folder_name,
            "search": folder_name,
            "url": "https://www.bing.com/images/search?q=%s" % folder_name.replace(' ', '%20') + '&qft=+filterui:photo-transparent',
            "similar_images": "",
            "image_directory": "20200403-0129",
            "limit": 2000,
            "print_urls": True,
            "size": "medium",
            "download": True,
            "format": "png",
            "output_directory": output_folder,
            "chromedriver": "C:/chromedriver/chromedriver.exe"
        }

        # print(f"Cleaning up folder before start...")
        # if silent_remove_of_folder(output_folder):
        #     print(f"Deleted {output_folder} folder")
        # else:
        #     print(f"Failed to delete {output_folder} folder")
        #     return
        # os.mkdir(output_folder)

        response = bing_scraper.googleimagesdownload()
        response.download(arguments)

    end_time = datetime.now().strftime("%H:%M:%S")
    print('End Time: ' + end_time)
    return


if __name__ == '__main__':
    start_download_images()
