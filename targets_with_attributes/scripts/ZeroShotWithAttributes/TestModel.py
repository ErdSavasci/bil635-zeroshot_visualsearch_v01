import sys
import platform
import numpy as np
import torch.nn as nn
import torch
import torchvision.transforms as transforms
import argparse

from torch.utils import data
from Utils import TargetDataset
from Utils import evaluate
from Utils import make_predictions
from Utils import build_model
from Utils import load_model

plt = platform.system()


def arg2bool(arg):
    if isinstance(arg, bool):
        return arg
    elif arg.lower() in ('yes', 'true', 'y', 't', 1):
        return True
    elif arg.lower() in ('no', 'false', 'n', 'f', 0):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def main():
    if plt == 'Windows':
        root_path = 'E:/BIL635-FinalProject/ErdSavasci-BIL635-ZeroShot_VisualSearch/targets_with_attributes' \
                    '/first_30_ones/'
    elif plt == 'Linux':
        root_path = '/home/erdsavasci/bil635-zeroshot_visualsearch/targets_with_attributes' \
                    '/first_30_ones/'
    else:
        sys.exit(0)

    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=arg2bool, nargs='?', const=True, default=True)
    args = parser.parse_args()
    args = vars(args)
    verbose = args['verbose']

    num_epochs = 1
    batch_size = 1
    curr_acc_test = 0.0
    output_filename = 'predictions.txt'

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    train_classes = np.array(np.genfromtxt(root_path + 'data/trainclasses.txt', dtype='str'))
    test_classes = np.array(np.genfromtxt(root_path + 'data/testclasses.txt', dtype='str'))
    classes = np.array(np.genfromtxt(root_path + 'data/classes.txt', dtype='str'))[:, -1]
    predicate_binary_mat = np.array(np.genfromtxt(root_path + 'data/predicate-matrix-binary.txt', dtype='int'))

    loaded_model = build_model(predicate_binary_mat.shape[1], False, False, device)
    loaded_model = load_model(loaded_model, root_path + 'output/models/model.bin', device)

    test_process_steps = transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor()
    ])
    test_dataset = TargetDataset(root_path, 'testclasses.txt', test_process_steps, False, False)
    single_train_classes_test_dataset = TargetDataset(root_path, 'trainclasses.txt', test_process_steps, True, False)
    single_test_classes_test_dataset = TargetDataset(root_path, 'testclasses.txt', test_process_steps, True, False)

    test_params = {'batch_size': batch_size, 'shuffle': True, 'num_workers': 3}

    # noinspection PyTypeChecker
    test_loader = data.DataLoader(test_dataset, **test_params)
    # noinspection PyTypeChecker
    single_train_classes_test_loader = data.DataLoader(single_train_classes_test_dataset, **test_params)
    # noinspection PyTypeChecker
    single_test_classes_test_loader = data.DataLoader(single_test_classes_test_dataset, **test_params)

    loaded_model.train(False)
    loaded_model.eval()

    with torch.no_grad():
        criterion = nn.BCELoss()
        epoch = 0
        total_steps = len(test_loader)

        for i, (images, features, img_names, indexes) in enumerate(test_loader):
            images = images.to(device)
            features = features.to(device).float()

            outputs = loaded_model(images)
            sigmoid_outputs = torch.sigmoid(outputs)
            loss = criterion(sigmoid_outputs, features)

            if verbose:
                print(
                    'LAST TEST ACC: {} Epoch [{}/{}], Step [{}/{}], Test Loss: {:.4f}'.format(curr_acc_test, epoch + 1,
                                                                                              num_epochs, i + 1,
                                                                                              total_steps, loss.item()))

    # Do some evaluations
    print('Evaluating:')
    curr_acc_test = evaluate(loaded_model, test_loader, device, classes, predicate_binary_mat, train_classes, 'test')
    print('Epoch [{}/{}] Approx. test accuracy: {}'.format(epoch + 1, num_epochs, curr_acc_test))

    single_curr_acc_train = evaluate(loaded_model, single_train_classes_test_loader, device, classes,
                                     predicate_binary_mat, train_classes, 'train')
    print('Epoch [{}/{}] Approx. single images training accuracy: {}'.format(epoch + 1, num_epochs,
                                                                             single_curr_acc_train))
    single_curr_acc_test = evaluate(loaded_model, single_test_classes_test_loader, device, classes,
                                    predicate_binary_mat, train_classes, 'test')
    print('Epoch [{}/{}] Approx. single images test accuracy: {}'.format(epoch + 1, num_epochs,
                                                                         single_curr_acc_test))

    # Make final predictions
    print('Making predictions:')
    make_predictions(root_path, loaded_model, test_loader, output_filename, device, predicate_binary_mat, classes,
                     train_classes, 'test', False, verbose)
    make_predictions(root_path, loaded_model, single_train_classes_test_loader, output_filename + '_single', device,
                     predicate_binary_mat, classes, train_classes, 'train', True, verbose)
    make_predictions(root_path, loaded_model, single_test_classes_test_loader, output_filename + '_single', device,
                     predicate_binary_mat, classes, train_classes, 'test', True, verbose)

    print('Predictions are written into files in ' + root_path + 'output/' + ' directory')


if __name__ == '__main__':
    main()
